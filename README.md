# Buell XB12Ss Chiptuning

## Binaries

* <b>BUIEB_ORG.bin</b> - <i>The origional fetched from the stock ECU.</i> This binary functions as backup.
* <b>BUIEB_TUN.bin</b> - <i>The binary used for tuning (Log-driving etc.).</i>
    The log data of this binary can be used top uptimize the fuel map.
    * Closed Loop
    * All functions disabled, e.g.: WOT enrichment, AFV and deceleration adjustment.
* <b>BUIEB_MOD.bin</b> - <i>The tuned (modified) binary for driving!</i> Open loop mode with most functions enabled and an optimized fuel map.

## Parts
<table>
<tr><th>Name</th> <th>Description</th> <th>Details</th></tr>
<tr><td>ECM Diagnostics Cable/Plug</td>
    <td>Colorscheme:<ul><li>Orange: ECM RX</li><li>Yellow: ECM TX</li><li>Black: ground</li></ul></td>
    <td>The diagnostics plug consists of three individual parts:
        <ul><li>Plug housing: DT06-4S-C015</li>
            <li>Wedge: W4S</li>
            <li>Contact socket: 0462-201-16141</li></ul>
            A.K.A.; Deutsch Connector 4-PIN
        </td></tr>
<!--
<tr><td><+Name+></td> <td><+Description+></td> <td><+Details+></td></tr>
-->
</table>

## Important data values
* <b>RPM</b> - <i>Rotations Per Minute</i>
    <br />
    In the fuel map these are defined in 13 colums:<br />
    800, 1000, 1350, 1900, 2400, 3400, 3900, 4400, 5000, 6000, 7000, 8000
* <b>TTP (TPS)</b> - <i>Throttle ... Position (Throttle Position Sensor)</i>
    <br />
    The TTP, measured by the TPS, is an 8bit value (0-255) representing the position of the throttle.
    In the fuel map this is defined in 12 rows:<br />
    10, 15, 20, 30, 40, 50, 60, 80, 100, 125, 175, 255
* <b>O2</b> - Lambda sensor value.
    <br />
    Jeej
* <b>AFV</b> - The AFV is a correction derived in the Closed Loop Learn (and Open Loop Learn)
    regime(s) and is applied in the Open Loop and Open Loop WOT control regimes.
* <b>EGO Corr.</b> - The EGO is a correction derived and applied during Closed Loop and Closed Loop Learn control regimes.
* <b>CLT</b> - Cylinder heat temp.
* <b>W</b> - Weight determines the importunateness of a value to a dataset.
    <br />
    Calculated with $w = \exp(-\Delta/Range)$
    ``` python
    np.exp(-(index_value - value) / (index_value - low))
    ```

## Data logging

## Logfile analysation
Obtain gear from ECM ratio output
``` c
void callGearRatio(void) {
  GearRatio = outArray[100];
#define Gear1 42
#define Gear2 61
#define Gear3 81
#define Gear4 98
#define Gear5 115
  if(Gear1 - 5 < GearRatio && GearRatio < Gear1 + 5)
    Gear = 1;
  else if(Gear2 - 5 < GearRatio && GearRatio < Gear2 + 5)
    Gear = 2;
  else if(Gear3 - 5 < GearRatio && GearRatio < Gear3 + 5)
    Gear = 3;
  else if(Gear4 - 5 < GearRatio && GearRatio < Gear4 + 5)
    Gear = 4;
  else if(Gear5 - 5 < GearRatio && GearRatio < Gear5 + 5)
    Gear = 5;
  else {
    Gear = 0;
  }
}
```

## Fuel map optimization

## Usefull links
<b>Buell Specific</b>
- [ECM Spy](http://www.ecmspy.com/)
    - [ECM Cable](http://www.ecmspy.com/steckermontage.shtml)
    - [EEPROM BUE1D](http://www.ecmspy.com/cgi-bin/ecm.cgi?ecm=49)
    - [Tuning Guide](http://www.ecmspy.com/tgv2/guide2.shtml)

<b>ECU Tuning Software</b>
- [TunerPro RT](https://www.tunerpro.net/)
    - [Configuration guide](http://badweatherbikers.com/buell/messages/32777/691713.html)
- [MAXXECU](https://www.maxxecu.com/)

