#!/usr/bin/env python3

from sys import exit
from typing import Union
import pandas
import numpy as np

from argparse import ArgumentParser


ARGPARSER = ArgumentParser()
ARGPARSER.add_argument(dest="file", help="Log file")
ARGPARSER.add_argument(
    "--generate-tune-data",
    dest="generate_tune_data",
    default=False,
    action="store_true",
    help="If this option is used, data-tables used for tuning fuel maps are generated.",
)
ARGPARSER.add_argument(
    "--upload-data",
    dest="upload_data",
    default=False,
    action="store_true",
    help="If this option is used, tune-data is generated and uploaded to the google-spreadsheets.",
)
ARGPARSER.add_argument(
    "--plot-stats",
    dest="plot_stats",
    default=False,
    action="store_true",
    help="If this option is used, some of the logged data is plotted.",
)
ARGPARSER.add_argument(
    "--clean-data",
    dest="clean_data",
    default=False,
    action="store_true",
    help="If this option is used, the data is 'cleaned' before being processed.",
)
ARGS = ARGPARSER.parse_args()


if not (ARGS.generate_tune_data or ARGS.upload_data or ARGS.plot_stats):
    print("Please use one of the program arguments to make it do something\n")
    ARGPARSER.print_help()
    exit(1)

DATA = pandas.read_csv(ARGS.file)
if ARGS.clean_data is True:
    # Remove all data where the engine temperature is below 100 degree celcius
    DATA = DATA[DATA["Engine Temp."] > 100]
    DATA = DATA[DATA["Engine Temp."] < 800]
    DATA = DATA[DATA["O2"] < 1]
    DATA = DATA[DATA["Air Temp."] < 100]

if ARGS.plot_stats is True:
    from matplotlib import pyplot as plt

    fig = plt.figure()
    gs = fig.add_gridspec(4, 2, hspace=0)
    axs = gs.subplots(sharex=True)

    def _redraw_legend(ax, sub_ax, legends, loc, title=None):
        legend = ax.legend(legends, [p.get_label() for p in legends], loc=loc, title=title)
        legend.remove()
        sub_ax.add_artist(legend)

    # RPM, TPP and Velocity
    p00 = axs[0, 0].plot(DATA["#"], DATA["Load"], label="Load/TPP", color="blue")
    p01 = axs[0, 0].plot(DATA["#"], DATA["VehicleSpeedSensor"], label="Velocity", color="blue", linestyle="dashed")
    axs[0, 0].set_ylim(-10, 265)
    axs0 = axs[0, 0].twinx()
    p02 = axs0.plot(DATA["#"], DATA["RPM"], label="RPM", color="red")
    axs0.set_ylim(-264, 7000)
    _redraw_legend(axs[0, 0], axs0, p00 + p01 + p02, "upper left")

    # Engine & Air temp. + Fan Duty
    p10 = axs[0, 1].plot(DATA["#"], DATA["Engine Temp."], label="Engine Temp. [C]", color="blue")
    p11 = axs[0, 1].plot(DATA["#"], DATA["Air Temp."], label="Air Temp. [C]", color="blue", linestyle="dashed")
    # axs[0, 1].set_ylim(-15, 400)
    axs1 = axs[0, 1].twinx()
    p12 = axs1.plot(DATA["#"], DATA["Fan Duty"], label="Fan Duty", color="red")
    axs1.set_ylim(-10, 265)
    _redraw_legend(axs[0, 1], axs1, p10 + p11 + p12, "upper right")

    # Fuel scalars and advance spark [degrees]
    p20 = axs[1, 0].plot(DATA["#"], DATA["Fuel Front"], label="Fuel Front", color="blue")
    p21 = axs[1, 0].plot(DATA["#"], DATA["Fuel Rear"], label="Fuel Rear", color="blue", linestyle="dashed")
    # axs[1, 0].set_ylim(-175, 265)
    axs2 = axs[1, 0].twinx()
    p22 = axs2.plot(DATA["#"], DATA["Spark Front"], label="Spark Front", color="red")
    p23 = axs2.plot(DATA["#"], DATA["Spark Rear"], label="Spark Rear", color="red", linestyle="dashed")
    # axs2.set_ylim(-10, 265)  ## TODO: Don't know the values for the sparks yet
    _redraw_legend(axs[1, 0], axs2, p20 + p21 + p22 + p23, "upper left")

    # Dynamic corrections 1
    p30 = axs[1, 1].plot(DATA["#"], DATA["OpenLoop Corr."], label="Open loop [%]", color="blue")
    p31 = axs[1, 1].plot(DATA["#"], DATA["AdaptiveFuelValue"], label="AFV [%]", color="blue", linestyle="dashed")
    p32 = axs[1, 1].plot(DATA["#"], DATA["ExchaustGasO2 Corr."], label="EGO [%]", color="blue", linestyle="dotted")
    # axs[1, 1].set_ylim(40, 160)
    axs3 = axs[1, 1].twinx()
    p33 = axs3.plot(DATA["#"], DATA["O2"], label="O2 [V]", color="red")
    # axs3.set_ylim(40, 160)  ## TODO: Don't know the values of O2 voltage yet
    _redraw_legend(axs[1, 1], axs3, p30 + p31 + p32 + p33, "upper right", "Fuel Corrections")

    # Dynamic corrections 2
    p40 = axs[2, 0].plot(DATA["#"], DATA["Accel Corr."], label="Accel [%]", color="blue")
    p41 = axs[2, 0].plot(DATA["#"], DATA["Decel Corr."], label="Decel [%]", color="blue", linestyle="dashed")
    axs4 = axs[2, 0].twinx()
    # axs[2, 0].set_ylim(40, 160)  ## TODO: Don't know the Accel and Decel correction values yet
    p42 = axs4.plot(DATA["#"], DATA["Idle Corr."], label="Idle [%]", color="red")
    p43 = axs4.plot(DATA["#"], DATA["WideOpenThrottle Corr."], label="WOT [%]", color="red", linestyle="dashed")
    # axs4.set_ylim(40, 160)
    _redraw_legend(axs[2, 0], axs4, p40 + p41 + p42 + p43, "upper left", title="Fuel Corrections")

    # Weather corrections
    p50 = axs[2, 1].plot(DATA["#"], DATA["Batt. Corr."], label="Batt. [ms]", color="blue")
    p51 = axs[2, 1].plot(DATA["#"], DATA["AirTemp Corr."], label="Air Temp. [%]", color="blue", linestyle="dashed")
    p52 = axs[2, 1].plot(DATA["#"], DATA["WarmUpEnrichment"], label="WarmUp [%]", color="blue", linestyle="dotted")
    axs5 = axs[2, 1].twinx()
    p53 = axs5.plot(DATA["#"], DATA["Baro ADC"], label="Baro. ADC", color="red")
    _redraw_legend(axs[2, 1], axs5, p50 + p51 + p52 + p53, "upper right", title="Fuel Corrections")

    p60 = axs[3, 0].plot(DATA["#"], DATA["Batt. Volt."], label="Battery [V]", color="blue")
    axs[3, 0].tick_params(axis="x", direction="in")
    axs6 = axs[3, 0].twinx()
    p61 = axs6.plot(DATA["#"], DATA["BankAngleSensor Volt."], label="BankAngle [V]", color="red")
    _redraw_legend(axs[3, 0], axs6, p60 + p61, "upper left")

    axs[3, 1].legend(title="Error codes")
    for i in range(5):
        axs[3, 1].plot(DATA["#"], DATA[f"Error Byte {i}"], label=f"Byte {i}")
    axs[3, 1].legend(title="Error codes", loc="upper right")

    for i in range(4):
        for j in range(2):
            axs[i, j].grid(which="both", axis="both", color="lightgrey", linestyle="dashed", linewidth=1)
            axs[i, j].tick_params(axis="y", labelcolor="blue", direction="in")
    for ax in [axs0, axs1, axs2, axs3, axs4, axs5, axs6]:
        ax.tick_params(axis="y", labelcolor="red", direction="in")
        ax.grid(which="both", axis="y", color="lightgrey", linestyle="dotted", linewidth=1)
    axs[3, 1].tick_params(color="black", labelright=True, direction="in")

    plt.subplots_adjust(left=0.033, bottom=0.038, right=0.965, top=0.985, wspace=0.124, hspace=0.11)
    plt.show()


if ARGS.generate_tune_data or ARGS.upload_data:

    # Define RPM and TPP (8 bit throtle-sensor) values of the fuel maps
    RPM = [800, 1000, 1350, 1900, 2400, 2900, 3400, 3900, 4400, 5000, 6000, 7000, 8000]
    TPP = [10, 15, 20, 30, 40, 50, 60, 80, 100, 125, 175, 255]

    # Define the dataframes of interest
    AFV = pandas.DataFrame(0.0, columns=RPM, index=TPP)
    O2 = pandas.DataFrame(0.0, columns=RPM, index=TPP)
    EGO = pandas.DataFrame(0.0, columns=RPM, index=TPP)
    CLT = pandas.DataFrame(0.0, columns=RPM, index=TPP)
    W = pandas.DataFrame(0.0, columns=RPM, index=TPP)

    def set_limits(array: list, i: int) -> list:
        """set_limits returns the lower and upper limits of the range belonging to the i'th value in the 'array'.

        The limits are determined by taking the middle value between the i'th value and the neighboring values
        (i-1 and i+1). At the first element the positive range is also used for the lower limit, and at the last value
        of the array the negative range is also used for the upper value.

        Args:
            array (list): List of values to set the boundaries for.
            i (int): The index to set the range for.

        Returns:
            list: list of two floats: lower and upper limits of the range.
        """
        if i == 0:
            high = (array[i] + array[i + 1]) / 2
            low = 2 * array[i] - high
        elif i == len(array) - 1:
            low = (array[i] + array[i - 1]) / 2
            high = 2 * array[i] - low
        else:
            low = (array[i] + array[i - 1]) / 2
            high = (array[i] + array[i + 1]) / 2
        return [low, high]

    def weight(index_value: float, low: float, high: float, value: float) -> float:
        """weight determines the importunateness of a value to a dataset.

        A value contributing to the i'th element of the array gets a contributing weight factor, being the
        exponent of minus the difference between the i'th element value and the value itself, divided by the range limit
        size. w = exp(-Delta/Range).

        Args:
            index_value (float): The value of the index in the RPM x TPP table.
            low (float): The lower limit of the range belonging to the index.
            high (float): The upper limit of the range belonging to the index.
            value (float): The value to determine the weight of.

        Returns:
            float: The Weight of the value.
        """
        if value < index_value:
            return np.exp(-(index_value - value) / (index_value - low))
        return np.exp(-(value - index_value) / (high - index_value))

    # Iterate over the RPM points in the fuel map
    for i_rpm, _rpm in enumerate(RPM):
        r_low, r_high = set_limits(RPM, i_rpm)
        data = DATA[DATA["RPM"] > r_low][DATA["RPM"] < r_high]
        # Iterate over the TPP points in the fuel map
        for i_tpp, _tpp in enumerate(TPP):
            t_low, t_high = set_limits(TPP, i_tpp)
            _data = data[data["Load"] > t_low][data["Load"] < t_high]

            # Calculate the weights, O2 and AFV values per RPM and TPP log
            for _, row in _data.iterrows():
                w = weight(_rpm, r_low, r_high, row["RPM"]) * weight(_tpp, t_low, t_high, row["Load"])
                W[_rpm][_tpp] += w
                O2[_rpm][_tpp] += w * row["O2"]
                AFV[_rpm][_tpp] += w * row["AdaptiveFuelValue"]
                EGO[_rpm][_tpp] += w * row["ExchaustGasO2 Corr."]
                CLT[_rpm][_tpp] += w * row["Engine Temp."]

            if W[_rpm][_tpp] > 0.0:
                O2[_rpm][_tpp] /= W[_rpm][_tpp]
                AFV[_rpm][_tpp] /= W[_rpm][_tpp]
                EGO[_rpm][_tpp] /= W[_rpm][_tpp]
                CLT[_rpm][_tpp] /= W[_rpm][_tpp]
            else:
                W[_rpm][_tpp] = np.nan
                O2[_rpm][_tpp] = np.nan
                AFV[_rpm][_tpp] = np.nan
                EGO[_rpm][_tpp] = np.nan
                CLT[_rpm][_tpp] = np.nan

    if ARGS.generate_tune_data is True:
        print("Tunings-data in tables - x-axis: RPM, y-axis: Load/TPP\n")
        print(f"O2\n{O2}")
        print(f"\nAFV\n{AFV}")
        print(f"\nEGO\n{EGO}")
        print(f"\nCLT\n{CLT}")

    if ARGS.upload_data is True:
        import gspread_pandas as gp
        from oauth2client.service_account import ServiceAccountCredentials

        def _eval(arg: str) -> Union[str, int, float]:
            """Function to parse string to float or integer. On ValueErrors return string itself.

            Args:
                arg (str): String to parse.

            Returns:
                Union[str, int,float]: Parsed string
            """
            if "." in arg:
                try:
                    return float(arg)
                except ValueError:
                    return arg
            else:
                try:
                    return int(arg)
                except ValueError:
                    return arg

        # The settings to push the values to a google-spreadsheet after calculation
        scope = ["https://spreadsheets.google.com/feeds", "https://www.googleapis.com/auth/drive"]
        creds = ServiceAccountCredentials.from_json_keyfile_name("./API_credentials.json", scope)
        spread = gp.Spread("Buell XB12Ss - Fuel Map Tuning", "TuningSheet", creds=creds)

        # Push the values of the AFV dataframe to the google spreadsheet in the cells P1:AC13
        spread.df_to_sheet(AFV.round(2))
        new_data = spread.sheet.range("A1:N13")
        afv_data = spread.sheet.range("P15:AC27")
        for i, d in enumerate(new_data):
            afv_data[i].value = _eval(d.value)
        spread.sheet.update_cells(afv_data)

        # Push the values of the EGO dataframe to the google spreadsheet in the cells A15:N27
        spread.df_to_sheet(EGO.round(2))
        new_data = spread.sheet.range("A1:N13")
        ego_data = spread.sheet.range("A15:N27")
        for i, d in enumerate(new_data):
            ego_data[i].value = _eval(d.value)
        spread.sheet.update_cells(ego_data)

        # Push the values of the W dataframe to the google spreadsheet in the cells P15:AC27
        spread.df_to_sheet(W.round(2))
        new_data = spread.sheet.range("A1:N13")
        w_data = spread.sheet.range("P1:AC13")
        for i, d in enumerate(new_data):
            w_data[i].value = _eval(d.value)
        spread.sheet.update_cells(w_data)

        # Push the values of the CLT dataframe to the google spreadsheet in the cells A29:N41
        spread.df_to_sheet(CLT.round(2))
        new_data = spread.sheet.range("A1:N13")
        w_data = spread.sheet.range("P29:AC41")
        for i, d in enumerate(new_data):
            w_data[i].value = _eval(d.value)
        spread.sheet.update_cells(w_data)

        # Push the values of the O2 dataframe to the google spreadsheet in the cells A1:N13
        spread.df_to_sheet(O2.round(2))

        # Set the right names for the tables in the spreadsheet
        spread.sheet.update_cell(1, 1, "O2")
        spread.sheet.update_cell(1, 16, "W")
        spread.sheet.update_cell(15, 1, "EGO")
        spread.sheet.update_cell(15, 16, "AFV")
        spread.sheet.update_cell(29, 16, "CLT")

        print("-" * 80 + f"\nGo to:\n{spread.url}")
