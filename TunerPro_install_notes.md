Using Tunerpro RT...

1. Download Tunerpro Rt from Tunerpro.net...

Get the plugin while there to read and write to the ECM.

2. Install Tunerpro Rt, unzip the plugin and place it in the created Tunerpro folder in your documents - plugin file...

3. Set up Tunerpro for first use...
A. Open Tunerpro and click tools (have your cable driver installed
in addition, have it plugged in.

B. Select Tools, click preferences, you get a popup box with two sides in it.
1. Data Acquisition,
a. select use plug in on the top selection
b. select Tunerpro Data Acqu I/O
c. click configure Plug-in Component (use your device manager to find the comport the cable is on.

Also, note each USB port will have a different port number, therefore, use the same port always to
link or change the port number to match each use.

2. Emulation,
A. select Emulation Plug-in Component and switch it to Buell EEprom reader/writer

B. Click The general tab, you have four selection spaces.
1. top left all boxes checked.
2. Bottom Left select: XDF, ADX, BIN
3. Top right select Diff. (edit- Compare), color table cells and Reverse Columns

C. Click Apply, Click OK

4. Click View on your Toolbar, then click tools and select, Basic, Tools, RT, Emulation, all three data Acquisition items.

5. Connect to bike and Turn on Key and run Switch.
A. If it did not auto connect, select the Grey Cable looking Item on the toolbar on the top.
B. it will beep when connected (If speakers are on)
C. You will see Initializing in the lower left of the screen and your firmware will show up.

6. Use this value to go to Xoptiinside.com to get your ADX and XDF_Lite as well as the links there to go to the
ECMspy.com site to get those versions.


7. Once downloaded all four versions. Please relocate the XDF and ADX into the documents folder
(Tunerpro folder and paste into Bin Definitions)

8. Open Tunerpro if not open select XDF on the tool bar.

A. Select XDF (either the lite or full version) and go to documents /Tunerpro/bin definitions and select
the XDF. Each time Tunerpro is open it will auto load this file from this point.
B. Select Acquisition, click load definition file and go to documents /Tunerpro/bin definitions, select
either mine with TPS, Codes and diag or ECMspys.

9.Once connected on the read/write side. you will have blue arrows up and down appear on the tool bar.

To Connect
A. up uploads to ECM and down downloads to Tunerpro RT
1. select the down arrow and it will read the ECM.
a. Click file once downloaded on the top left on the toolbar. use Save bin as and name this
file as anyname_stock.bin then do it once again saving as the ECM name you chose
my stock ECM_1.bin. adding a _1 to each new saved version will help in the future.

10. To use the live data and tps/clear codes/ diag side/
A. Click tools, then click release emulation... It will state, "Hardware not found"...


11. With your adx Loaded. now click the two blue arrows right and left facing. it will connect and the yellow box
showing connected will appear in blue.

A. To data log, select Acquisition on toolbar, scroll down and select record.
B. To stop select Acquisition on toolbar, scroll down and select stop and a popup box will appear. Name your
file for a one-time use.
C. To play it back, select Acquisition on toolbar, scroll down and select play and move through to the items you
want to see or setup... data dash, List Items, history and data monitors on the toolbar top center...
D. These are fully adjustable in edit adx under Acquisition to match your ECM and needs.

12. To return to the read write just select the two arrows to disconnect and select the grey plug.

A. To make changes open desired item click on cells and type in new value or paste it in then
click the save cd on that box in the top left. Note: this saves it to the file in Tunerpro not
in the ECM or in the documents...
B. once all changes are made and saved to the file in the program, click files the save as and
name it with your chose name and the _#...
C. now you upload the new file to the bike with the upload arrow. A popup box will appear with
the loaded firmware name. Please, make 100% it matches your actual ECM. If not you
have to revert to one that does match and start over.

Notes.

2007 down bikes use the tps reset feature.

1. Back out tps knob fully and flip throttle so it is 100% closed.
2. go to live data side with my ADX and connect. on the tool bar select the drop down and select reset TPS.
click send... done
3. Go to dash view on my adx and turn in knob until you start the bike and it idles at 1000 rpm.
a. take bike out and ride it until warm (15 to 20 minutes) then adjust tps rpm to 1000/975 rpm.
Do not worry about the degree as the manual had to use a value for EPA/ISO reasons this is now set to
the true value to match your bike.

Tps reset is throttle controlled on 2008 up...
